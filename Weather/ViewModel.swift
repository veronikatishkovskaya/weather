import Foundation
import UIKit
import CoreLocation

class ViewModel: NSObject, CLLocationManagerDelegate {
    
    var weather = Bindable<Objects?>(nil)
    var temperature = Bindable<Double?>(nil)
    var date = Bindable<Int?>(nil)
    var timezone = Bindable<Int?>(nil)
    var condition = Bindable<String?>(nil)
    var feelsLike = Bindable<Double?>(nil)
    var sunrise = Bindable<Int?>(nil)
    var sunset = Bindable<Int?>(nil)
    var humidity = Bindable<Int?>(nil)
    var windSpeed = Bindable<Double?>(nil)
    var pressure = Bindable<Int?>(nil)
    var icon = Bindable<String?>(nil)
    let locationManager = CLLocationManager()
    var locationLatitude = Double()
    var locationLongtitude = Double()
    var defaultCityLoaded = false
    var city = String()
    
    override init() {
        super.init()
    }
    
    func getCurrentWeather() {
        ApiManager.shared.getWeather(city: self.city) { (result) in
            self.weather.value = result
            
            guard let lists = result?.list else {return}
            let list = lists[0]
            self.date.value = list.dt
            
            guard let main = list.main else {return}
            self.temperature.value = main.temp
            self.feelsLike.value = main.feels_like
            self.humidity.value = main.humidity
            self.pressure.value = main.pressure
            
            guard let weathers = list.weather else {return}
            for weather in weathers {
                self.condition.value = weather.description
                self.icon.value = weather.icon
            }
            
            guard let wind = list.wind else {return}
            self.windSpeed.value = wind.speed
            
            guard let city = result?.city else {return}
            self.sunrise.value = city.sunrise
            self.sunset.value = city.sunset
            self.timezone.value = city.timezone
        }
    }
    
    func getLocation() {
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        DispatchQueue.main.async {
            let location: CLLocationCoordinate2D = manager.location!.coordinate
            self.locationLatitude = location.latitude
            self.locationLongtitude = location.longitude
            
            if self.defaultCityLoaded == false {
                let newLocation = locations.last
                let geocoder = CLGeocoder()
                geocoder.reverseGeocodeLocation(newLocation!) { (placemarks, error) in
                    if let error = error {
                        debugPrint(error.localizedDescription)
                    }
                    if let placemarks = placemarks {
                        if placemarks.count > 0 {
                            let placemarks = placemarks[0]
                            if let city = placemarks.locality {
                                self.city = city.localized
                                self.defaultCityLoaded = true
                                self.getCurrentWeather()
                            }
                        }
                    }
                }
            }
        }
    }
    
    
}
extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}

