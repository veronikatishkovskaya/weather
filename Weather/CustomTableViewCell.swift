
import UIKit

class CustomTableViewCell: UITableViewCell {
    //MARK: - IBOutlets
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherConditionLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    //MARK: - var
    var timezone = 0
    //MARK: - Life func
    func configure(with object: Weather) {
        let temperature = object.main
        self.temperatureLabel.text = "\(Int(temperature?.temp ?? 0))°"
        
        let date = object.dt
        let time = convertDatatoLocalTimezone(date: date!, timezone: timezone )
        self.dateLabel.text = getDayForDate(Date(timeIntervalSince1970: Double(time)))
        
        guard let weather = object.weather else {return}
        for condition in weather {
            self.weatherConditionLabel.text = condition.description
            
            guard let icon = condition.icon else {return}
            let url = URL(string: "http://openweathermap.org/img/wn/\(icon)@2x.png")
            guard let data = try? Data(contentsOf: url!) else { return }
            self.weatherIcon.image = UIImage(data: data)
        }
    }
    
}
//MARK: - Extensions
extension CustomTableViewCell {
    func getDayForDate(_ date: Date?) -> String {
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEE HH:mm"
        return formatter.string(from: inputDate)
    }
}

extension CustomTableViewCell {
    func convertDatatoLocalTimezone(date: Int, timezone: Int) -> Int {
        let currentTimezone = TimeZone.current.secondsFromGMT()
        let convertedTime = date - (currentTimezone - timezone)
        return convertedTime
    }
}
