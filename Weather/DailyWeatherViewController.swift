import UIKit

class DailyWeatherViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nabigationBar: UINavigationBar!
    //MARK: - var
    var dailyWeatherArray: [Weather] = []
    var timezoneValue = 0
    var city = ""
    //MARK: - Life func
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.reloadData()
        self.nabigationBar.topItem?.title = "\(city)"
    }
}
//MARK: - Extensions
extension DailyWeatherViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.dailyWeatherArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        cell.timezone = self.timezoneValue
        cell.configure(with: dailyWeatherArray[indexPath.row])
        return cell
    }
}

extension DailyWeatherViewController {
    func getDayForDate(_ date: Date?) -> String {
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter.string(from: inputDate)
    }
}

extension DailyWeatherViewController {
    func convertDatatoLocalTimezone(date: Int, timezone: Int) -> Int {
        let currentTimezone = TimeZone.current.secondsFromGMT()
        let convertedTime = date - (currentTimezone - timezone)
        return convertedTime
    }
}
