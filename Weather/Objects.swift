import Foundation

class Objects: Codable {
    var list: [Weather]?
    var city: City?
}

class Weather: Codable {
    var dt: Int?
    var main: MainWeather?
    var weather: [WeatherCondition]?
    var wind: Wind?
    
}

class City: Codable {
    var name: String?
    var sunrise: Int?
    var sunset: Int?
    var timezone: Int?
}

class MainWeather: Codable {
    var temp: Double?
    var feels_like: Double?
    var pressure: Int?
    var humidity: Int?
}

class WeatherCondition: Codable {
    var main: String?
    var description: String?
    var icon: String?
}

class Wind: Codable {
    var speed: Double?
}


