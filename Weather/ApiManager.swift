import Foundation
import CoreLocation

class ApiManager {
    
    static let shared = ApiManager()
    
    private init() {}
    
    func getWeather(city: String, completion: @escaping ((Objects?) -> ())) {
        var objects: Objects?
        var urlComponents = URLComponents()
        urlComponents.scheme = "https"
        urlComponents.host = "api.openweathermap.org"
        urlComponents.path = "/data/2.5/forecast"
        urlComponents.queryItems = [URLQueryItem(name: "q", value: city),
                                    URLQueryItem(name: "appid", value: "07d4838b3f6736d2e67abd4f608a88ac"),
                                    URLQueryItem(name: "units", value: "metric"),
        ]
        
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error == nil, let data = data {
                do {
                    objects = try JSONDecoder().decode(Objects.self, from: data)
                    completion(objects)
                } catch {
                    print(error)
                }
            }
        }
        task.resume()
    }
    
}


