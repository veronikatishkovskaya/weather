import UIKit

class ViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var navigationBar: UINavigationBar!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var weatherIcon: UIImageView!
    @IBOutlet weak var feelsLikeTemperatureLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var windSpeedLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var sunriseLabel: UILabel!
    @IBOutlet weak var sunsetLabel: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var hideView: UIView!
    //MARK: - var
    var viewModel: ViewModel?
    var dailyWeatherArray: [Weather] = []
    var timezone = 0
    var shareDataArray: [String] = []
    //MARK: - VC Life Func
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel = ViewModel()
        self.viewModel?.getLocation()
        self.setCurrentData()
    }
    //MARK: - IBAction
    @IBAction func shareWeatherData(_ sender: UIButton) {
        self.shareWeatherForecast()
    }
    //MARK: - Life Func
    func setCurrentData() {
        DispatchQueue.main.async {
            let controller = self.tabBarController?.viewControllers
            let dailyController = controller![1] as! DailyWeatherViewController
            
            self.viewModel?.weather.bind({ (weather) in
                DispatchQueue.main.async {
                    self.startLoading()
                    guard let weather = weather else {return}
                    guard let list = weather.list else {return}
                    self.dailyWeatherArray = list
                    self.cityNameLabel.text = self.viewModel?.city
                    dailyController.city = self.cityNameLabel.text!
                    dailyController.dailyWeatherArray = self.dailyWeatherArray
                }
            })
            self.viewModel?.timezone.bind({ (timezone) in
                DispatchQueue.main.async {
                    guard let timezone = timezone else {return}
                    self.timezone = timezone
                    dailyController.timezoneValue = self.timezone
                }
            })
            self.viewModel?.date.bind({ (date) in
                DispatchQueue.main.async {
                    let date = Date()
                    let formatter = DateFormatter()
                    formatter.dateStyle = .full
                    let timeInterval = date.timeIntervalSince1970
                    let convertedDate = Int(timeInterval)
                    let timezoneDate = self.convertDatatoLocalTimezone(date: convertedDate, timezone: self.timezone)
                    let currentDate = Date(timeIntervalSince1970: Double(timezoneDate))
                    self.dateLabel.text = formatter.string(from: currentDate)
                }
            })
            self.viewModel?.condition.bind({ (condition) in
                DispatchQueue.main.async {
                    guard let condition = condition else {return}
                    self.conditionLabel.text = condition
                }
            })
            self.viewModel?.temperature.bind({ (temperature) in
                DispatchQueue.main.async {
                    guard let temperature = temperature else {return}
                    self.temperatureLabel.text = "\(Int(temperature))°"
                }
            })
            self.viewModel?.feelsLike.bind({ (feelsLike) in
                DispatchQueue.main.async {
                    guard let feelsLike = feelsLike else {return}
                    self.feelsLikeTemperatureLabel.text = "feels like: \(Int(feelsLike))°"
                }
            })
            self.viewModel?.humidity.bind({ (humidity) in
                DispatchQueue.main.async {
                    guard let humidity = humidity else {return}
                    self.humidityLabel.text = "\(humidity)%"
                }
            })
            self.viewModel?.pressure.bind({ (pressure) in
                DispatchQueue.main.async {
                    guard let pressure = pressure else {return}
                    self.pressureLabel.text = "\(pressure)hPa"
                }
            })
            self.viewModel?.windSpeed.bind({ (windSpeed) in
                DispatchQueue.main.async {
                    guard let windSpeed = windSpeed else {return}
                    let speed = windSpeed * 3.6
                    self.windSpeedLabel.text = "\(Int(speed))km/h"
                }
            })
            self.viewModel?.icon.bind({ (icon) in
                DispatchQueue.main.async {
                    guard let icon = icon else {return}
                    let url = URL(string: "http://openweathermap.org/img/wn/\(icon)@2x.png")
                    guard let data = try? Data(contentsOf: url!) else { return }
                    self.weatherIcon.image = UIImage(data: data)
                }
            })
            self.viewModel?.sunrise.bind({ (sunrise) in
                DispatchQueue.main.async {
                    guard let sunrise = sunrise else {return}
                    let timeSunrise = self.convertDatatoLocalTimezone(date: sunrise, timezone: self.timezone )
                    let sunriseTime = self.getDayForDate(Date(timeIntervalSince1970: Double(timeSunrise)))
                    self.sunriseLabel.text = "\(sunriseTime)"
                }
            })
            self.viewModel?.sunset.bind({ (sunset) in
                DispatchQueue.main.async {
                    guard let sunset = sunset else {return}
                    let timeSunset = self.convertDatatoLocalTimezone(date: sunset, timezone: self.timezone )
                    let sunsetTime = self.getDayForDate(Date(timeIntervalSince1970: Double(timeSunset)))
                    self.sunsetLabel.text = "\(sunsetTime)"
                    self.finishLoading()
                }
            })
       }
    }
    
    func shareWeatherForecast() {
        self.shareDataArray.append(self.dateLabel.text!)
        self.shareDataArray.append("city: \(self.cityNameLabel.text!)")
        self.shareDataArray.append("sunrise: \(self.sunriseLabel.text!)")
        self.shareDataArray.append("sunset: \(self.sunsetLabel.text!)")
        self.shareDataArray.append("temperature: \(self.temperatureLabel.text!)")
        self.shareDataArray.append(self.feelsLikeTemperatureLabel.text!)
        self.shareDataArray.append("humidity: \(self.humidityLabel.text!)")
        self.shareDataArray.append("pressure: \(self.pressureLabel.text!)")
        
        let activityController = UIActivityViewController(activityItems: shareDataArray, applicationActivities: nil)
        present(activityController, animated: true, completion: nil)
    }
    
    func startLoading() {
        self.hideView.isHidden = false
        self.activityIndicator.startAnimating()
        self.view.addSubview(self.activityIndicator)
    }
    
    func finishLoading() {
        self.hideView.isHidden = true
        self.activityIndicator.stopAnimating()
        self.activityIndicator.hidesWhenStopped = true
    }
    
}
//MARK: - Extensions
extension ViewController {
    func getDayForDate(_ date: Date?) -> String {
        guard let inputDate = date else {
            return ""
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: inputDate)
    }
}

extension ViewController {
    func convertDatatoLocalTimezone(date: Int, timezone: Int) -> Int {
        let currentTimezone = TimeZone.current.secondsFromGMT()
        let convertedTime = date - (currentTimezone - timezone)
        return convertedTime
    }
}

